/* 2. Задано масив цілих чисел.
Вивести масив в оберненому порядку, а потім видалити з нього повторні входження кожного елемента.
 */

package com.automation.qa;

public class Lab2 {

    private static Integer[] myArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    public static void main(String[] args) {
        makeArrayBackwords(myArray);
        System.out.println(ifContains(myArray, 3));

        for (Integer i : deleteRepeatedElement(myArray)) {
            System.out.println(i);
        }
    }

    private static void makeArrayBackwords(Integer[] arr){
        int len = (arr.length % 2 == 0) ? arr.length / 2 : arr.length / 2 + 1; //length even / odd

        for (int i = 0; i < len; i++) {
            int buffer = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = buffer;
        }
    }

    private static boolean ifContains(Integer[] arr, int val){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(val)) return true;
        }
        return false;
    }

    private static Integer[] deleteRepeatedElement(Integer[] arr){
        //set "repeated" element with "null"
        int numOfNull = 0; // number of repeated el
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (arr[i] == arr[j] && arr[i] != null){
                    arr[i] = null;
                    numOfNull++;
                }
            }
        }

        Integer[] newArr = new Integer[arr.length - numOfNull]; // length - quantity of nulls

        int i = 0;
        int j = 0;
        while (j < newArr.length){
            if(arr[i] != null){
                newArr[j] = arr[i];
                j++;
                i++;
            } else {
                i++;
            }
        }
        return newArr;
    }
}
